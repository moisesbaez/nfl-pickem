"use strict";

Template.login.events({
    'submit .login-form': function (event) {
        event.preventDefault();

        var username = $('.login-username').val(),
            password = $('.login-password').val();

        if (username === "") {
            Materialize.toast("Please enter a valid username", 3000);
            return false;
        }

        if (password === "") {
            Materialize.toast("Please enter a valid password", 3000);
            return false;
        }

        Meteor.loginWithPassword(username, password, function (error) {
            if (error) {
                Materialize.toast("Invalid username or password", 3000);
            } else if (Meteor.user().username) {
                Router.go("gamemaster");
            }
        });

        return false;
    }
});

Template.register.events({
    'submit .register-form': function (event) {
        event.preventDefault();

        var username = $('.register-username').val(),
            password = $('.register-password').val(),
            passwordRetype = $('.register-password-retype').val();

        if (username === "") {
            Materialize.toast("Please enter a valid username", 3000);
            return false;
        }

        if (password === "") {
            Materialize.toast("Please enter a valid password", 3000);
            return false;
        } else if (password !== passwordRetype) {
            Materialize.toast("Passwords do not match, please retype your password", 3000);
            return false;
        }

        Meteor.call("findUser", username, function (error, result) {
            if (result !== undefined) {
                Materialize.toast("Username exists, please choose another username", 3000);
                return false;
            }

            Accounts.createUser({
                username: username,
                email: "",
                password: password,
                profile: null
            });
        });

        return false;
    }
});

Template.currentWeek.events({
    'submit .current-week': function (event) {
        event.preventDefault();

        var selectedWeek = $(".current-week select").val(),
            document = Collections.Season.findOne({
                currentSeason: {
                    $exists: true
                }
            });

        Collections.Season.update({
            _id: document._id
        }, {
            $set: {
                currentWeek: selectedWeek
            }
        });

        $(".current-week select").val("");

        return false;
    }
});

Template.createGames.events({
    'submit .create-game': function (event) {
        event.preventDefault();

        var homeTeam = $('.home-team').val(),
            awayTeam = $('.away-team').val(),
            currentSeason = Collections.Season.findOne({
                currentSeason: { $exists: true }
            }),
            season = Collections.Season.findOne({
                season: {
                    $exists: true
                }
            }),
            seasonUpdate = {};

        if (homeTeam === null || awayTeam === null) {
            return false;
        }

        seasonUpdate[currentSeason.currentWeek] = {home: homeTeam, away: awayTeam, winner: ""};
        
        Collections.Season.update({_id: season._id}, {$push: seasonUpdate});
        
        return false;
    }
});