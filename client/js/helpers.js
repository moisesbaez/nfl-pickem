"use strict";

Template.currentWeek.helpers({
    currentWeek: function () {
        var document = Collections.Season.findOne({currentSeason: {$exists: true}});
        return document.currentWeek;
    },
    
    weeks: function () {
        return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17];
    }
});

Template.createGames.helpers({
    teamList: function () {
        return Teams;
    },

    refreshSelect: function () {
        $(document).ready(function () {
            $('select').material_select();
        });
    }
});

Template.pickem.helpers({
    gameList: function () {
        var document = Collections.Season.findOne({currentSeason: {$exists: true}}),
            season = Collections.Season.findOne({season: document.currentSeason});
        
    }
});