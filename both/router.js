"use strict";

Router.route('/', {
    name: "login",
    template: "login"
});

Router.route('/register', {
    name: "register",
    template: "register"
});

Router.route('/gamemaster', {
    name: "gamemaster",
    template: "gamemaster"
});

Router.onBeforeAction(function () {
    var user = Meteor.user();
    if (user === null || user === undefined || user.username !== "admin") {
        Router.go("login");
    } else {
        this.next();
    }
}, {
    only: ['gamemaster']
});