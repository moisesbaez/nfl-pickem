Collections = {};

Collections.Pickem = new Mongo.Collection("pickem");
Collections.Season = new Mongo.Collection("season");

Teams = ["Baltimore Ravens", "Cincinnati Bengals", "Cleveland Browns", "Pittsburgh Steelers", "Chicago Bears", "Detroit Lions", "Green Bay Packers", "Minnesota Vikings", "Houston Texans", "Indianapolis Colts", "Jacksonville Jaguars", "Tennessee Titans", "Atlanta Falcons", "Carolina Panthers", "New Orleans Saints", "Tampa Bay Buccaneers", "Buffalo Bills", "Miami Dolphins", "New England Patriots", "New York Jets", "Dallas Cowboys", "New York Giants", "Philadelphia Eagles", "Washington Redskins", "Denver Broncos", "Kansas City Chiefs", "Oakland Raiders", "San Diego Chargers", "Arizona Cardinals", "San Francisco 49ers", "Seattle Seahawks", "St. Louis Rams"];