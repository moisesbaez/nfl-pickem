"use strict";

Meteor.startup(function () {
    var season = Collections.Season.find({}),
        admin = Accounts.findUserByUsername("admin");
    
    if (season.count() === 0) {
        Collections.Season.insert({currentSeason: 1, currentWeek: 1});
        Collections.Season.insert({season: 1});
    }

    if (admin === undefined) {
        Accounts.createUser({
            username: "admin",
            email: "",
            password: "admin",
            profile: null
        });
    }
});