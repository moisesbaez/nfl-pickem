Meteor.methods({
    findUser: function (username) {
        var result = Accounts.findUserByUsername(username);
        return result.username;
    }
});